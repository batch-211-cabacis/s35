// ODM - odject document mapper

// Mongoose - is a data library

// Schemas - representation of document structure, contains document's properties and data types

// Models - programming interface for querying or manipulating database. Mongoose model contains methods that simplify operations


const express = require("express");

// Mongoose is a package that allows creation of Schemas to model our data structures
// It also has access to a number of methods for manipulating our data base
const mongoose = require("mongoose");

const app = express();

const port = 3001;

// MongoDB connection
// connect the data base by passing in our connection string, remember to replace the password and database names with actual values
//  {newUserParser:true} allows us to avoid any current and future errors while connecting to MongoDB

// Syntax
	// mongoose.connect("<MongoDb connection string>", {useNewUrlParser:true})

	// Connect to MongoDB atlas

	mongoose.connect("mongodb+srv://admin123:admin123@project0.jelgw36.mongodb.net/s35?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

// Set notifications for connection success or failure
// connection to data base
// allows us to handle errors when the initial connection is established
// workds with the on and once Mongoose methods

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
// if a connection error occured, output in the console
// console.error.bind(console) allows us to print errors in our terminal

db.once("open", () => console.log("We're connected to the cloud database"));


// Mongoose Schemas
// schemas determine the structure of documents to be written in the database
// they act as blueprint to our data
// The"new" keyword is used to create breand new schema
// use the Schema() constructor of the Mongoose module to create a new Schema object


const taskSchema = new mongoose.Schema({
	// define the fields with their corresponding data type

	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// Models
// uses cshemas and are used to create/instantiate objects that correspond to the schema
// Server>Schema(blueprint)>Database>Collection
// models must be in singular forms and capital

const Task = mongoose.model("Task", taskSchema)


// Steup for allowing the server to handle data from requests

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// Create a new task
// Business Logic
/*
	1. add a functionality to check if there are duplicate tasks 
	- if the task already exist in the database we return an error 
	- if the task does not exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. create a new task object with a "name" field or property 
	4. The "status" property does not need to be provided because our schema defaults it to pending upon creation of an object
*/

app.post("/tasks", (req, res) => {
	Task.findOne({name: req.body.name}, (err,result) => {
			if(result != null && result.name == req.body.name){
				return res.send("Duplicate task found");
			}else {
				let newTask = new Task ({
					name: req.body.name,
				});
				newTask.save((saveErr, savedTask) => {
					if(saveErr){
						return console.log(saveErr);
					}else{
						return res.status(201).send("New task created");
					}
				})
			}
	})	
})

// Getting all the tasks
// Business Logic
/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If not errors are encountered, send a success status back to the client/postman and return an array of documnets
*/

app.get("/tasks", (req, res) => {
	Task.find({}, (err,result) => {
		if(err){
			return console.log(err);
		}else {
			return res.status(200).json({
				data: result
			})
		}
	})
})


// ACTIVTY

// 1. Create user schema
const userSchema = new mongoose.Schema({
	username: String,
	password: String
});


// 2. Create a user model
const User = mongoose.model("User", userSchema)


// 3. Create a POST route that will access the /signup toue that will create a user
app.post("/signup", (req, res) => {
	User.findOne({name: req.body.username}, (err,result) => {
			if(result != null && result.name == req.body.username){
				return res.send("User already exist");
			}else {
				let newUser = new User ({
					username: req.body.username,
					password: req.body.password

				});
				newUser.save((saveErr, savedUser) => {
					if(saveErr){
						return console.log(saveErr);
					}else{
						return res.status(201).send("New user added");
					}
				})
			}
	})	
})


// Registering a user
// Business Logic

app.get("/signup", (req, res) => {
	User.find({}, (err,result) => {
		if(err){
			return console.log(err);
		}else {
			return res.status(200).json({
				data: result
			})
		}
	})
})



app.listen(port,() => console.log(`Server running at port ${port}`));